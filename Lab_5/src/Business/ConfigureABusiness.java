/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Employee.Employee;
import Business.Organization.AdminOrganization;
import Business.Organization.LabOrganization;
import Business.UserAccount.UserAccount;
import Business.Organization.DoctorOrganization;

/**
 *
 * @author ran
 */
public class ConfigureABusiness {
    
    public static Business configure(){
        // Three roles: LabAssistant, Doctor, Admin
        
        Business business = Business.getInstance();
        
        AdminOrganization adminOrganization = new AdminOrganization();
        LabOrganization labOrganiziation = new LabOrganization();
        DoctorOrganization doctorOrganization = new DoctorOrganization();
        
        business.getOrganizationDirectory().getOrganizationList().add(adminOrganization);
        business.getOrganizationDirectory().getOrganizationList().add(labOrganiziation);
        business.getOrganizationDirectory().getOrganizationList().add(doctorOrganization);
        
        //employee
        Employee employee = new Employee();
        employee.setName("Aelx");
        Employee employee2 = new Employee();
        employee2.setName("Ben");
        Employee employee3 = new Employee();
        employee3.setName("Jackie");
        
        //usercount
        UserAccount account = new UserAccount();
        account.setUsername("admin");
        account.setPassword("admin");
        UserAccount account2 = new UserAccount();
        account2.setUsername("lab");
        account2.setPassword("lab");
        UserAccount account3 = new UserAccount();
        account3.setUsername("doctor");
        account3.setPassword("doctor");
        
        account.setRole("Admin");
        account2.setRole("Lab");
        account3.setRole("Doctor");
        account.setEmployee(employee);
        account2.setEmployee(employee2);
        account3.setEmployee(employee3);
        
        
        adminOrganization.getEmployeeDirectory().getEmployeeList().add(employee);
        adminOrganization.getEmployeeDirectory().getEmployeeList().add(employee2);
        adminOrganization.getEmployeeDirectory().getEmployeeList().add(employee3);
        
        adminOrganization.getUserAccountDirectory().getUserAccountList().add(account);
        adminOrganization.getUserAccountDirectory().getUserAccountList().add(account2);
        adminOrganization.getUserAccountDirectory().getUserAccountList().add(account3);
        
        return business;
    }
    
}
